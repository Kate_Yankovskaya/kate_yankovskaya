package choose_of_tests;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;

import dictionary.ChooseOfTheDictionary;
import dictionary.Dictionary;
import tests.FirstTest;
import tests.SecondTest;
import tests.ThirdTest;

import javax.swing.JButton;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.awt.event.ActionEvent;
import java.awt.Font;
import javax.swing.JLabel;
import java.awt.Color;

public class Main {

	private JFrame frame;
	private String username = "";
	private static int settings = 2;
	private static int amountOfWordSetting = 15;
	private JLabel lblusername = null;
	
	private static ArrayList<HashMap<String, ArrayList<String>>> learning = new ArrayList<>();
	private static ArrayList<HashMap<String, ArrayList<String>>> learned = new ArrayList<>();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Main window = new Main();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Main() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setResizable(false);
		frame.setBounds(100, 100, 500, 500);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.setTitle("Main page");
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(240, 230, 140));
		panel.setBounds(0, 0, 494, 471);
		frame.getContentPane().add(panel);
		panel.setLayout(null);
		
		lblusername = new JLabel(username);
		lblusername.setFont(new Font("Tahoma", Font.PLAIN, 26));
		lblusername.setBounds(15, 16, 128, 30);
		panel.add(lblusername);
		
		JButton btnsettings = new JButton("");
		btnsettings.setBackground(new Color(255, 250, 205));
		try {
			btnsettings.setIcon(new ImageIcon(ImageIO.read(new File("Files/settings.png"))));
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		btnsettings.setBounds(413, 16, 50, 50);
		panel.add(btnsettings);
		btnsettings.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				Setting s = new Setting();
				s.change(username);
				frame.dispose();
			}
		});
		
		JButton btnexit = new JButton("");
		btnexit.setBackground(new Color(255, 250, 205));
		try {
			btnexit.setIcon(new ImageIcon(ImageIO.read(new File("Files/exit.png"))));
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		btnexit.setBounds(413, 378, 50, 50);
		panel.add(btnexit);
		btnexit.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				upDate();
				frame.dispose();
				System.exit(0);
			}
		});
		
		JButton btntest1 = new JButton("Test 1");
		btntest1.setBackground(new Color(255, 250, 205));
		btntest1.setFont(new Font("Yu Gothic UI Semibold", Font.PLAIN, 36));
		btntest1.setBounds(100, 85, 278, 60);
		panel.add(btntest1);
		btntest1.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				frame.dispose();
				FirstTest ft = new FirstTest();
				ft.change(username);
			}
		});
		
		JButton btntest2 = new JButton("Test 2");
		btntest2.setBackground(new Color(255, 250, 205));
		btntest2.setFont(new Font("Yu Gothic UI Semibold", Font.PLAIN, 36));
		btntest2.setBounds(100, 160, 278, 60);
		panel.add(btntest2);
		btntest2.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				frame.dispose();
				SecondTest st = new SecondTest();
				st.change(username);
			}
		});
		
		JButton btntest3 = new JButton("Test 3");
		btntest3.setBackground(new Color(255, 250, 205));
		btntest3.setFont(new Font("Yu Gothic UI Semibold", Font.PLAIN, 36));
		btntest3.setBounds(100, 235, 278, 60);
		panel.add(btntest3);
		btntest3.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				frame.dispose();
				ThirdTest tt = new ThirdTest();
				tt.change(username);
			}
		});
		
		JButton btnDictionaries = new JButton("Dictionary");
		btnDictionaries.setBackground(new Color(255, 250, 205));
		btnDictionaries.setFont(new Font("Yu Gothic UI Semibold", Font.PLAIN, 36));
		btnDictionaries.setBounds(100, 310, 278, 60);
		panel.add(btnDictionaries);
		btnDictionaries.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				frame.setVisible(false);
				ChooseOfTheDictionary cod = new ChooseOfTheDictionary();
				cod.change(username);
			}
		});
		
	}
	
	public void change() {
		frame.setVisible(true);
	}
	
	public void upDate() {
		UsersInfoAboutDictionaries uiad = null;
		try {
			uiad = new UsersInfoAboutDictionaries(username);
		} catch (Exception e) {
			e.printStackTrace();
		}
		uiad.updateDict(username);
	}
	
	public void setUsername(String username) 
	/* ��������� ����� ������������ ��� ���������� ������ � ��� */{
		this.username = username;
		lblusername.setText(username);
	}
	
	public void setSettingAmountOfWords(int setting) {
		amountOfWordSetting = setting;
		if (amountOfWordSetting != getLerning().size()) {
			Dictionary.reFill();
		}
	}
	
	public static int getSettingAmountOfWords() {
		return amountOfWordSetting;
	}
	
	public void setSettingType(int setting) {
		settings = setting;
	}
	
	public static int getSettings() {
		return settings;
	}
	
	public static void setLerning(ArrayList<HashMap<String, ArrayList<String>>> l) {
		learning = l;
	}
	
	public static ArrayList<HashMap<String, ArrayList<String>>> getLerning() {
		return learning;
	}
	
	public static void setLearned(ArrayList<HashMap<String, ArrayList<String>>> l) {
		learned = l;
	}
	
	public static ArrayList<HashMap<String, ArrayList<String>>> getLerned() {
		return learned;
	}

	public void setDict() {
		UsersInfoAboutDictionaries uiad = null;
		try {
			uiad = new UsersInfoAboutDictionaries(username);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		learning = uiad.getLerning();
		learned = uiad.getLearned();
	}
}