package dictionary;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;

import choose_of_tests.Main;

import javax.swing.JButton;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.swing.JLabel;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import java.awt.Color;

public class ChooseOfTheDictionary {

	private JFrame frmDictionaries;
	private String username = "";
	private static int click = 0;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ChooseOfTheDictionary window = new ChooseOfTheDictionary();
					window.frmDictionaries.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public ChooseOfTheDictionary() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmDictionaries = new JFrame();
		frmDictionaries.setTitle("Dictionaries");
		frmDictionaries.setResizable(false);
		frmDictionaries.setBounds(100, 100, 486, 486);
		frmDictionaries.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmDictionaries.getContentPane().setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(240, 230, 140));
		panel.setBounds(0, 0, 480, 457);
		frmDictionaries.getContentPane().add(panel);
		panel.setLayout(null);
		
		JLabel lblChooseTypeOf = new JLabel("Choose type of the dictionary that ");
		lblChooseTypeOf.setForeground(new Color(0, 0, 255));
		lblChooseTypeOf.setFont(new Font("Georgia", Font.ITALIC, 25));
		lblChooseTypeOf.setBounds(52, 24, 387, 44);
		panel.add(lblChooseTypeOf);
		
		JLabel lblWouldLikeTo = new JLabel("you would like to look through");
		lblWouldLikeTo.setForeground(new Color(0, 0, 255));
		lblWouldLikeTo.setFont(new Font("Georgia", Font.ITALIC, 25));
		lblWouldLikeTo.setBounds(74, 55, 339, 41);
		panel.add(lblWouldLikeTo);
		
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////		
		
		JButton btnAllWords = new JButton("All words");
		btnAllWords.setBackground(new Color(255, 250, 205));
		btnAllWords.setForeground(new Color(0, 0, 205));
		btnAllWords.setFont(new Font("Georgia", Font.PLAIN, 36));
		btnAllWords.setBounds(97, 116, 278, 60);
		panel.add(btnAllWords);
		btnAllWords.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if (click == 0) {
					MainDictionary md = new MainDictionary();
					md.change();
					click++;
				} 				
			}
		});
		
		JButton btnLearned = new JButton("Learned");
		btnLearned.setBackground(new Color(255, 250, 205));
		btnLearned.setForeground(new Color(0, 0, 205));
		btnLearned.setFont(new Font("Georgia", Font.PLAIN, 36));
		btnLearned.setBounds(97, 205, 278, 60);
		panel.add(btnLearned);
		
		btnLearned.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if (click == 0) {
					if (Main.getLerned().isEmpty()) {
						Error er = new Error();
						click++;
						er.change();
					} else {
						Learned l = new Learned();
						click++;
						l.change();
					}
				} 		
			}
		});
		
		JButton btnInProcess = new JButton("In process");
		btnInProcess.setBackground(new Color(255, 250, 205));
		btnInProcess.setForeground(new Color(0, 0, 205));
		btnInProcess.setFont(new Font("Georgia", Font.PLAIN, 36));
		btnInProcess.setBounds(97, 294, 278, 60);
		panel.add(btnInProcess);
		
		btnInProcess.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if (click == 0) {
					Learning l = new Learning();
					click++;
					l.change();
				} 
			}
		});
		
		JButton btnExit = new JButton("");
		btnExit.setBackground(new Color(255, 250, 205));
		try {
			btnExit.setIcon(new ImageIcon(ImageIO.read(new File("Files/exit.png"))));
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		btnExit.setBounds(415, 380, 50, 50);
		panel.add(btnExit);
		btnExit.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				Main m = new Main();
				m.change();
				m.setUsername(username);
				frmDictionaries.dispose();				
			}
		});
		
	}
	
	public static void setClick(int c) {
		click = c;
	}
	
	public void change(String username) {
		frmDictionaries.setVisible(true);
		this.username = username;
	}	
}