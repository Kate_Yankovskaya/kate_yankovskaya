package dictionary;

import java.awt.Color;
import java.awt.EventQueue;
import javax.swing.DefaultListModel;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import choose_of_tests.Main;

import java.awt.Font;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

public class Learning {

	private JFrame frame;
	private final JPanel panel = new JPanel();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Learning window = new Learning();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Learning() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(new Color(240, 230, 140));
		frame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosed(WindowEvent arg0) {
				ChooseOfTheDictionary.setClick(0);
			}
		});
		frame.setResizable(false);
		frame.setBounds(100, 100, 882, 881);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		panel.setBounds(0, 0, 878, 844);
		frame.getContentPane().add(panel);
		frame.setTitle("Dictionary");

		ArrayList<HashMap<String, ArrayList<String>>> dict = Main.getLerning();
		
		DefaultListModel<String> listModel = new DefaultListModel<>();
        for (int i = 0; i < dict.size(); i++) {
        	HashMap<String, ArrayList<String>> h = dict.get(i);
        	
        	String justOne = " ";
        			
        	Set<String> keys = h.keySet(); 
			for (String key: keys) { 
				justOne+=(key + " - ");
				ArrayList<String> a = h.get(key);
				for (int j = 0; j < a.size(); j++) {
					if (j == a.size()-1) {
						justOne+=a.get(j);
					} else {
						justOne+=(a.get(j) + ", ");
					}
				}
			}
			
			listModel.addElement(justOne);
        }
		panel.setLayout(null);
		JList<String> list = new JList<>(listModel);
		list.setFont(new Font("Tahoma", Font.PLAIN, 36));
	    JScrollPane scrollableList = new JScrollPane(list);
	    scrollableList.setBounds(0, 0, 878, 844);
	    
	    panel.add(scrollableList);
	    
	    if (list.isSelectedIndex(0)) {
	    	panel.setBackground(Color.BLACK);
	    }
		}

		public void change() {
			frame.setVisible(true);
		}
}
