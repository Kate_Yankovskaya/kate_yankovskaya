package dictionary;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import choose_of_tests.Main;


public class Dictionary implements Serializable{

	private static final long serialVersionUID = 1L;

	private static ArrayList<HashMap<String, ArrayList<String>>> mainDictionary = new ArrayList<>(); // �������� �������
	private File f = null; // ��������� ����
	
	public Dictionary(int i) {
		
		if (i == 1) {
			f = new File("Files/Dictionaries/mydictionary2.txt");
		} else {
			f = new File("Files/Dictionaries/mydictionary.txt");
		}
		
		try {
			fillMainDictionary();
		} catch (ClassNotFoundException | IOException e) {
			e.printStackTrace();
		}
		
		checkOnLearnedLearning();
	}
	
	@SuppressWarnings("unchecked")
	public void fillMainDictionary() throws ClassNotFoundException, IOException {
	/* ��������� �������� �� ����� � ���������� ������ */		
		ObjectInputStream ois = null;
		
		FileInputStream fis = new FileInputStream(f);
		ois = new ObjectInputStream(fis);
		ArrayList<HashMap<String, ArrayList<String>>> p = (ArrayList<HashMap<String, ArrayList<String>>>) ois.readObject();
		mainDictionary = p;
			
		ois.close();

	}
	
	public HashMap<String, ArrayList<String>> getWordFromMainDictionary(int numberOfTheWord) {
	/* ����������� �������� ����� �������� �� ������� �� �������, ��� ������� */	
		return mainDictionary.get(numberOfTheWord);
	}
	
	public int getLength() {
		return mainDictionary.size();
	}
	
	public static void checkOnLearnedLearning() {
	// ������� ����, ������� � ��������� ���������
		
		ArrayList<HashMap<String, ArrayList<String>>> learning = Main.getLerning();
		for (int i = 0; i < learning.size(); i++) {
			if (mainDictionary.contains(learning.get(i))) {
				mainDictionary.remove(learning.get(i));
			}
		}
		
		learning = Main.getLerned();
		for (int i = 0; i < learning.size(); i++) {
			if (mainDictionary.contains(learning.get(i))) {
				mainDictionary.remove(learning.get(i));
			}
		}
	}
	
   public static void reFill() {
   // ���������� �� ������ ���������� ����
	   ArrayList<HashMap<String, ArrayList<String>>> a = Main.getLerning();
	   if (Main.getSettingAmountOfWords() < a.size()) {
		   int m = a.size() - Main.getSettingAmountOfWords();
		   for (int i = 1; i < m; i++) {
			   a.remove(new Random().nextInt(Main.getLerning().size()));
		   }
	   } else {
		   int m = Main.getSettingAmountOfWords() - a.size();
		   for (int i = 1; i < m; i++) {
			   a.add(mainDictionary.get(new Random().nextInt(mainDictionary.size())));
		   }
	   }
	   
	   checkOnLearnedLearning();
	   
	   Main.setLerning(a);
   }
}