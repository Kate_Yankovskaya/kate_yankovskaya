package autor_registr;

import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.Color;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

public class Registration {

	private JFrame frame;
	private JPasswordField passwordField = null;
	private JTextField username = null;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Registration window = new Registration();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public Registration() {
		initialize();
	}

	private void initialize() {	
		frame = new JFrame();
		frame.getContentPane().setBackground(new Color(240, 230, 140));
		frame.setResizable(false);
		frame.setBounds(100, 100, 500, 500);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setTitle("Registration");
		frame.getContentPane().setLayout(null);
		
		username = new JTextField();
		username.setFont(new Font("Georgia", Font.PLAIN, 36));
		username.setBounds(123, 169, 277, 51);
		frame.getContentPane().add(username);
		
		passwordField = new JPasswordField();
		passwordField.setFont(new Font("Tahoma", Font.PLAIN, 36));
		passwordField.setBounds(123, 234, 277, 51);
		frame.getContentPane().add(passwordField);
		
		JLabel lblNewLabel = new JLabel("username");
		lblNewLabel.setFont(new Font("Georgia", Font.PLAIN, 20));
		lblNewLabel.setBounds(15, 187, 97, 20);
		frame.getContentPane().add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("password");
		lblNewLabel_1.setFont(new Font("Georgia", Font.PLAIN, 20));
		lblNewLabel_1.setBounds(15, 241, 97, 25);
		frame.getContentPane().add(lblNewLabel_1);
		
		JLabel label = new JLabel("");
		label.setForeground(Color.RED);
		label.setFont(new Font("Georgia", Font.ITALIC, 20));
		label.setBounds(73, 378, 411, 32);
		frame.getContentPane().add(label);
		
		/////////////////////////////////////////////////////////////
		
		JButton btnOK = new JButton("OK");
		btnOK.setBackground(new Color(255, 250, 205));
		btnOK.setFont(new Font("Georgia", Font.PLAIN, 26));
		btnOK.setBounds(188, 301, 152, 51);
		frame.getContentPane().add(btnOK);
		
		ActionListener l = new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				char [] a = passwordField.getPassword();
				
				if (String.valueOf(a).isEmpty() || username.getText().isEmpty()) {
					label.setText("Please, enter your username or password");
				} else {
					Authorization auto = new Authorization();
					if (auto.isIn(username.getText())) {
						label.setText("Please, enter another username");
					} else {
						try {
							auto.addNewUser(username.getText(), String.valueOf(a));
						} catch (IOException e) {} catch (ClassNotFoundException e) {}
						changeFrameToAuto();
					}
				}	
			}
		};
		btnOK.addActionListener(l);
		
		JLabel lblRegistration = new JLabel("            Registration");
		lblRegistration.setFont(new Font("Georgia", Font.ITALIC, 40));
		lblRegistration.setBounds(31, 49, 463, 65);
		frame.getContentPane().add(lblRegistration);
	
	}
			
	public void changeFrameToAuto() {
		Autorization a = new Autorization();
		a.change();
		frame.setVisible(false);
	} 

	public void change() {
		frame.setVisible(true);
	}
}