package tests;

import java.awt.EventQueue;

import javax.swing.ButtonGroup;
import javax.swing.JFrame;
import java.awt.Color;
import javax.swing.JPanel;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;
import java.util.Set;

import javax.swing.JRadioButton;

import choose_of_tests.Main;
import dictionary.Dictionary;

import javax.swing.JButton;

public class SecondTest implements ActionListener {

	private JFrame frmSecondTest;
	protected static int score = 0;
	protected JRadioButton russian1, russian2, russian3, russian4, russian5;
	private JButton btnDone;
	private JLabel answerText;
	private JLabel english1;
	
	private Dictionary d = new Dictionary(Main.getSettings());
	private TestsImplementation sti = new TestsImplementation(5);
	private ArrayList<String> englishWord = new ArrayList<>(); // ������ ����
	private ArrayList<ArrayList<String>> rusWords = new ArrayList<ArrayList<String>>(); // ������ ���������
	protected static ArrayList<String> englishAnswerWord = new ArrayList<>(); // ������ ���� �������
	protected static ArrayList<ArrayList<String>> rusAnswerWords = new ArrayList<ArrayList<String>>(); // ������ ������� �������
	protected static ArrayList<Integer> engAns = new ArrayList<>();
	protected static ArrayList<Integer> rusAns = new ArrayList<>();
	private ArrayList<Integer> numbers = new ArrayList<Integer>();
	private int k = 0;
	protected static int action = 0;
	private String username = "";
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					SecondTest window = new SecondTest();
					window.frmSecondTest.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public SecondTest() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		
		int j = 0;
		collectWords();
		System.out.println(action);
		
		frmSecondTest = new JFrame();
		frmSecondTest.setResizable(false);
		frmSecondTest.setBackground(new Color(240, 240, 240));
		frmSecondTest.setTitle("Second Test");
		frmSecondTest.setBounds(100, 100, 615, 350);
		frmSecondTest.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmSecondTest.getContentPane().setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(255, 228, 196));
		panel.setForeground(Color.BLACK);
		panel.setBounds(0, 0, 609, 321);
		frmSecondTest.getContentPane().add(panel);
		panel.setLayout(null);
		
		double i = (Math.random() * (englishWord.size()));
		k = (int) i;
		
		english1 = new JLabel(englishWord.get(k));
		english1.setForeground(new Color(139, 0, 139));
		english1.setFont(new Font("Tahoma", Font.PLAIN, 20));
		english1.setBounds(10, 104, 223, 41);
		english1.setBackground(Color.PINK);
		panel.add(english1);
		collectAnswerWord(k);
		engAns.add(k);
		
		ButtonGroup group = new ButtonGroup();
		
		j = generateNumber();
		russian1 = new JRadioButton(rusWords.get(j).get(0), false);
		russian1.setBackground(new Color(255, 222, 173));
		if (sti.scan(rusWords.get(j)) != 1) {
			russian1 = new JRadioButton(rusWords.get(j).get(0) + ", " + rusWords.get(j).get(1));
		}
		russian1.setBounds(239, 7, 364, 46);
		russian1.setFont(new Font("Tahoma", Font.PLAIN, 18));
		russian1.setBackground(new Color(255, 228, 196));
		russian1.setForeground(new Color(148, 0, 211));
		panel.add(russian1);
		group.add(russian1);
		
		j = generateNumber();
		russian2 = new JRadioButton(rusWords.get(j).get(0),false);
		if (sti.scan(rusWords.get(j)) != 1) {
			russian2 = new JRadioButton(rusWords.get(j).get(0) + ", " + rusWords.get(j).get(1));
		}	
		russian2.setBounds(239, 56, 364, 46);
		russian2.setFont(new Font("Tahoma", Font.PLAIN, 18));
		russian2.setBackground(new Color(255, 228, 196));
		russian2.setForeground(new Color(148, 0, 211));
		panel.add(russian2);
		group.add(russian2);
		
		j = generateNumber();
		russian3 = new JRadioButton(rusWords.get(j).get(0),false);
		if (sti.scan(rusWords.get(j)) != 1) {
			russian3 = new JRadioButton(rusWords.get(j).get(0) + ", " + rusWords.get(j).get(1));
		}
		russian3.setBounds(239, 105, 364, 46);
		russian3.setFont(new Font("Tahoma", Font.PLAIN, 18));
		russian3.setBackground(new Color(255, 228, 196));
		russian3.setForeground(new Color(148, 0, 211));
		panel.add(russian3);
		group.add(russian3);
		
		j = generateNumber();
		russian4 = new JRadioButton(rusWords.get(j).get(0), false);
		if (sti.scan(rusWords.get(j)) != 1) {
			russian4 = new JRadioButton(rusWords.get(j).get(0) + ", " + rusWords.get(j).get(1));
		}	
		russian4.setBounds(239, 154, 364, 46);
		russian4.setFont(new Font("Tahoma", Font.PLAIN, 18));
		russian4.setBackground(new Color(255, 228, 196));
		russian4.setForeground(new Color(148, 0, 211));
		panel.add(russian4);
		group.add(russian4);

		j = generateNumber();
		russian5 = new JRadioButton(rusWords.get(j).get(0),false);
		if (sti.scan(rusWords.get(j)) != 1) {
			russian5 = new JRadioButton(rusWords.get(j).get(0) + ", " + rusWords.get(j).get(1));
		}
		russian5.setBounds(239, 203, 364, 46);
		russian5.setFont(new Font("Tahoma", Font.PLAIN, 18));
		russian5.setBackground(new Color(255, 228, 196));
		russian5.setForeground(new Color(148, 0, 211));
		panel.add(russian5);
		group.add(russian5);
		
		russian1.addActionListener(this);
		russian2.addActionListener(this);
		russian3.addActionListener(this);
		russian4.addActionListener(this);
		russian5.addActionListener(this);
		
		answerText = new JLabel("");
		answerText.setForeground(new Color(46, 139, 87));
		answerText.setFont(new Font("Tahoma", Font.BOLD, 18));
		answerText.setBounds(55, 256, 110, 36);
		panel.add(answerText);
		
		btnDone = new JButton("Next word");
		btnDone.setBackground(new Color(255, 240, 245));
		btnDone.setForeground(new Color(72, 61, 139));
		btnDone.setFont(new Font("Tahoma", Font.BOLD, 14));
		btnDone.setEnabled(false);
		btnDone.addActionListener(new ActionListener() { 
			public void actionPerformed(ActionEvent arg0) {
				if( action == 4){
					checkUsersDict(action);
					englishWord.clear();
					rusWords.clear();
					numbers.clear();
					frmSecondTest.dispose();
					SecondTextAnswers sta = new SecondTextAnswers();
					sta.frmAnswer.setVisible(true);
					sta.addUser(username);
				}
				if (action<4){
					checkUsersDict(action);
					englishWord.clear();
					rusWords.clear();
					numbers.clear();
					frmSecondTest.dispose();
					sti = new TestsImplementation(5);
					initialize();
					frmSecondTest.setVisible(true);
					action++;
				}
			}
		});
			
		btnDone.setBounds(413, 256, 130, 36);
		panel.add(btnDone);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		
		if (russian1.isSelected()){
			russian2.setEnabled(false);
			russian3.setEnabled(false);
			russian4.setEnabled(false);
			russian5.setEnabled(false);
			btnDone.setEnabled(true);			
			if (sti.scan(rusWords.get(k)) == 2){
				if ((russian1.getText() == rusWords.get(k).get(0)) || (russian1.getText().equals((rusWords.get(k).get(0) + ", " + rusWords.get(k).get(1))))) {
					answerText.setText("True");
					rusAns.add(1);
					score++;
				} else {
					answerText.setText("False");
					answerText.setForeground(Color.RED);
					rusAns.add(0);
				}
			} else {
				if ((russian1.getText() == rusWords.get(k).get(0))) {
					answerText.setText("True");
					rusAns.add(1);
					score++;
				} else {
					rusAns.add(0);
					answerText.setText("False");
					answerText.setForeground(Color.RED);
				}
			}
		}
		
		if (russian2.isSelected()){
			russian1.setEnabled(false);
			russian3.setEnabled(false);
			russian4.setEnabled(false);
			russian5.setEnabled(false);
			btnDone.setEnabled(true);
			if (sti.scan(rusWords.get(k)) == 2){
				if ((russian2.getText() == rusWords.get(k).get(0)) || (russian2.getText().equals((rusWords.get(k).get(0) + ", " + rusWords.get(k).get(1))))) {
					answerText.setText("True");
					rusAns.add(1);
					score++;
				} else {
					rusAns.add(0);
					answerText.setText("False");
					answerText.setForeground(Color.RED);
				}
			} else {
				if ((russian2.getText() == rusWords.get(k).get(0))) {
					answerText.setText("True");
					rusAns.add(1);
					score++;
				} else {
					rusAns.add(0);
					answerText.setText("False");
					answerText.setForeground(Color.RED);
				}
			}
		}
		
		if (russian3.isSelected()){
			russian2.setEnabled(false);
			russian1.setEnabled(false);
			russian4.setEnabled(false);
			russian5.setEnabled(false);
			btnDone.setEnabled(true);
			if (sti.scan(rusWords.get(k)) == 2){
				if ((russian3.getText() == rusWords.get(k).get(0)) || (russian3.getText().equals((rusWords.get(k).get(0) + ", " + rusWords.get(k).get(1))))) {
					answerText.setText("True");
					rusAns.add(1);
					score++;
				} else {
					rusAns.add(0);
					answerText.setText("False");
					answerText.setForeground(Color.RED);
				}
			} else {
				if ((russian3.getText() == rusWords.get(k).get(0))) {
					answerText.setText("True");
					rusAns.add(1);
					score++;
				} else {
					rusAns.add(0);
					answerText.setText("False");
					answerText.setForeground(Color.RED);
				}
			}
		}
		
		if (russian4.isSelected()){
			russian2.setEnabled(false);
			russian3.setEnabled(false);
			russian1.setEnabled(false);
			russian5.setEnabled(false);
			btnDone.setEnabled(true);
			if (sti.scan(rusWords.get(k)) == 2){
				if ((russian4.getText() == rusWords.get(k).get(0)) || (russian4.getText().equals((rusWords.get(k).get(0) + ", " + rusWords.get(k).get(1))))) {
					answerText.setText("True");
					rusAns.add(1);
					score++;
				} else {
					rusAns.add(0);
					answerText.setText("False");
					answerText.setForeground(Color.RED);
				}
			} else {
				if ((russian4.getText() == rusWords.get(k).get(0))) {
					answerText.setText("True");
					rusAns.add(1);
					score++;
				} else {
					rusAns.add(0);
					answerText.setText("False");
					answerText.setForeground(Color.RED);
				}
			}
		}
		
		if (russian5.isSelected()){
			russian2.setEnabled(false);
			russian3.setEnabled(false);
			russian4.setEnabled(false);
			russian1.setEnabled(false);
			btnDone.setEnabled(true);
			if (sti.scan(rusWords.get(k)) == 2){
				if ((russian5.getText() == rusWords.get(k).get(0)) || (russian5.getText().equals((rusWords.get(k).get(0) + ", " + rusWords.get(k).get(1))))) {
					answerText.setText("True");
					rusAns.add(1);
					score++;
				} else {
					rusAns.add(0);
					answerText.setText("False");
					answerText.setForeground(Color.RED);
				}
			} else {
				if ((russian5.getText() == rusWords.get(k).get(0))) {
					answerText.setText("True");
					rusAns.add(1);
					score++;
				} else {
					rusAns.add(0);
					answerText.setText("False");
					answerText.setForeground(Color.RED);
				}
			}
		}
	}

	
    public void collectWords() {
	// ���������� ������� ���� � ������� ������ �� ���������
		ArrayList<HashMap<String, ArrayList<String>>> dict = sti.getDict();
		
		for (int i = 0; i < dict.size(); i++) {
			HashMap<String, ArrayList<String>> onePairOfWords = dict.get(i);
			Set<String> keys = onePairOfWords.keySet();
			for (String key: keys) { 
				englishWord.add(key);
				rusWords.add(onePairOfWords.get(key));
			}
		}
	}
  
    public void collectAnswerWord(int k) {
	// ���������� ������� ������� � ������� ������� �� ���������
    	ArrayList<HashMap<String, ArrayList<String>>> dict = sti.getDict();

		HashMap<String, ArrayList<String>> onePairOfWords = dict.get(k);
		Set<String> keys = onePairOfWords.keySet();
		for (String key: keys) { 
			englishAnswerWord.add(key);
			rusAnswerWords.add(onePairOfWords.get(key));
		}
	}
	
	public int generateNumber() {
	// ��������� ������� ������� ���� � radiobutton
		Random r = new Random();
		int count = 0;
		do {
			count = (int) r.nextInt(englishWord.size());
		} while (numbers.contains(count));
		numbers.add(count);	
		return count;
	}
	
	public void change(String username) {
		frmSecondTest.setVisible(true);
		this.username = username;
	}
	
	public HashMap<String, ArrayList<String>> createAnswers(int i) {
		HashMap<String, ArrayList<String>> h = new HashMap<>();
		h.put(englishAnswerWord.get(i), rusAnswerWords.get(i));
		return h;
	}

	
	@SuppressWarnings("static-access")
	public void checkUsersDict(int i) {
	// ������ ���� � ��������
		if (rusAns.get(i) == 1) {
			ArrayList<HashMap<String, ArrayList<String>>> a = Main.getLerning();
			a.remove(createAnswers(i));
			a.add(d.getWordFromMainDictionary(new Random().nextInt(d.getLength())));

			ArrayList<HashMap<String, ArrayList<String>>> b = Main.getLerned();
			b.add(createAnswers(i));
			d.checkOnLearnedLearning();
			System.out.println("+");
		}
	}
}