package tests;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;
import java.util.Set;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JTextField;

import choose_of_tests.Main;
import dictionary.Dictionary;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.SystemColor;

public class ThirdTest {

	private JFrame frmThirdTest;
	private JTextField eng1, eng2, eng3, eng4, eng5;
	private JLabel engWord1, engWord2, engWord3, engWord4, engWord5, engWord6, engWord7, engWord8, engWord9, engWord10;
	private JLabel rusWord1, rusWord2, rusWord3, rusWord4, rusWord5;
	private JLabel label;
	private JButton btn1, btn2, btn3, btn4, btn5;
	private JButton btnDone, Exit;
	private JLabel label_0, label_1, label_2, label_3, label_4;
	private JLabel lblNewLabel;
	
	private Dictionary d = new Dictionary(Main.getSettings());
	private TestsImplementation tti = new TestsImplementation(10);
	private ArrayList<String> englishWord = new ArrayList<>(); // ������ ����
	private ArrayList<ArrayList<String>> rusWords = new ArrayList<ArrayList<String>>(); // ������ ���������
	private ArrayList<Integer> numbers = new ArrayList<Integer>();
	private ArrayList<String> englishAnswerWord = new ArrayList<>(); // ������ ���� �������
	private static int score = 0;
	private String username = "";
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ThirdTest window = new ThirdTest();
					window.frmThirdTest.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public ThirdTest() {
		try {
			initialize();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Initialize the contents of the frame.
	 * @throws IOException 
	 */
	private void initialize() throws IOException {
		int i1 = 1;
		int i = 0;
		int j = 0;
		collectWords();
		
		frmThirdTest = new JFrame();
		frmThirdTest.setResizable(false);
		frmThirdTest.setTitle("ThirdTest");
		frmThirdTest.setBounds(100, 100, 580, 493);
		frmThirdTest.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmThirdTest.getContentPane().setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(173, 216, 230));
		panel.setBounds(0, 0, 574, 464);
		frmThirdTest.getContentPane().add(panel);
		panel.setLayout(null);
		
		engWord1 = new JLabel(i1 + ") " + englishWord.get(i));
		engWord1.setForeground(new Color(75, 0, 130));
		engWord1.setFont(new Font("Tahoma", Font.PLAIN, 17));
		engWord1.setBounds(10, 35, 256, 28);
		panel.add(engWord1); i++; i1++;
		
		engWord2 = new JLabel(i1 + ") " + englishWord.get(i));
		engWord2.setForeground(new Color(75, 0, 130));
		engWord2.setFont(new Font("Tahoma", Font.PLAIN, 17));
		engWord2.setBounds(10, 74, 256, 28);
		panel.add(engWord2); i++; i1++;
		
		engWord3 = new JLabel(i1 + ") " + englishWord.get(i));
		engWord3.setFont(new Font("Tahoma", Font.PLAIN, 17));
		engWord3.setForeground(new Color(75, 0, 130));
		engWord3.setBounds(10, 113, 256, 28);
		panel.add(engWord3); i++; i1++;
		
		engWord4 = new JLabel(i1 + ") " + englishWord.get(i));
		engWord4.setFont(new Font("Tahoma", Font.PLAIN, 17));
		engWord4.setForeground(new Color(75, 0, 130));
		engWord4.setBounds(10, 152, 256, 28);
		panel.add(engWord4); i++; i1++;
		
		engWord5 = new JLabel(i1 + ") " + englishWord.get(i));
		engWord5.setForeground(new Color(75, 0, 130));
		engWord5.setFont(new Font("Tahoma", Font.PLAIN, 17));
		engWord5.setBounds(10, 191, 256, 28);
		panel.add(engWord5); i++; i1++;
		
		engWord6 = new JLabel(i1 + ") " + englishWord.get(i));
		engWord6.setFont(new Font("Tahoma", Font.PLAIN, 17));
		engWord6.setForeground(new Color(75, 0, 130));
		engWord6.setBounds(276, 35, 262, 28);
		panel.add(engWord6); i++; i1++;
		
		engWord7 = new JLabel(i1 + ") " + englishWord.get(i));
		engWord7.setForeground(new Color(75, 0, 130));
		engWord7.setFont(new Font("Tahoma", Font.PLAIN, 17));
		engWord7.setBounds(276, 74, 273, 28);
		panel.add(engWord7); i++; i1++;
		
		engWord8 = new JLabel(i1 + ") " + englishWord.get(i));
		engWord8.setFont(new Font("Tahoma", Font.PLAIN, 17));
		engWord8.setForeground(new Color(75, 0, 130));
		engWord8.setBounds(276, 113, 273, 28);
		panel.add(engWord8); i++; i1++;
		
		engWord9 = new JLabel(i1 + ") " + englishWord.get(i));
		engWord9.setForeground(new Color(75, 0, 130));
		engWord9.setFont(new Font("Tahoma", Font.PLAIN, 17));
		engWord9.setBounds(276, 152, 273, 28);
		panel.add(engWord9); i++; i1++;
		
		engWord10 = new JLabel(i1 + ") " + englishWord.get(i));
		engWord10.setForeground(new Color(75, 0, 130));
		engWord10.setFont(new Font("Tahoma", Font.PLAIN, 17));
		engWord10.setBounds(276, 191, 278, 28);
		panel.add(engWord10);
		
		j = generateNumber();
		rusWord1 = new JLabel(rusWords.get(j).get(0));
		if (tti.scan(rusWords.get(j)) != 1) {
			rusWord1 = new JLabel(rusWords.get(j).get(0) + ", " + rusWords.get(j).get(1));
		}
		rusWord1.setBounds(10, 230, 256, 28);
		rusWord1.setForeground(new Color(199, 21, 133));
		rusWord1.setFont(new Font("Tahoma", Font.PLAIN, 18));
		panel.add(rusWord1);
		englishAnswerWord.add(englishWord.get(j));
		System.out.println(englishAnswerWord.get(0));
		
		j = generateNumber();
		rusWord2 = new JLabel(rusWords.get(j).get(0));
		if (tti.scan(rusWords.get(j)) != 1) {
			rusWord2 = new JLabel(rusWords.get(j).get(0) + ", " + rusWords.get(j).get(1));
		}
		rusWord2.setBounds(10, 269, 256, 28);
		rusWord2.setForeground(new Color(199, 21, 133));
		rusWord2.setFont(new Font("Tahoma", Font.PLAIN, 18));
		panel.add(rusWord2);
		englishAnswerWord.add(englishWord.get(j));
		System.out.println(englishAnswerWord.get(1));
		
		j = generateNumber();
		rusWord3 = new JLabel(rusWords.get(j).get(0));
		if (tti.scan(rusWords.get(j)) != 1) {
			rusWord3 = new JLabel(rusWords.get(j).get(0) + ", " + rusWords.get(j).get(1));
		}
		rusWord3.setBounds(10, 308, 256, 28);
		rusWord3.setForeground(new Color(199, 21, 133));
		rusWord3.setFont(new Font("Tahoma", Font.PLAIN, 18));
		panel.add(rusWord3);
		englishAnswerWord.add(englishWord.get(j));
		System.out.println(englishAnswerWord.get(2));
		
		j = generateNumber();
		rusWord4 = new JLabel(rusWords.get(j).get(0));
		if (tti.scan(rusWords.get(j)) != 1) {
			rusWord4 = new JLabel(rusWords.get(j).get(0) + ", " + rusWords.get(j).get(1));
		}
		rusWord4.setBounds(10, 347, 256, 28);
		rusWord4.setForeground(new Color(199, 21, 133));
		rusWord4.setFont(new Font("Tahoma", Font.PLAIN, 18));
		panel.add(rusWord4);
		englishAnswerWord.add(englishWord.get(j));
		System.out.println(englishAnswerWord.get(3));
		
		j = generateNumber();
		rusWord5 = new JLabel(rusWords.get(j).get(0));
		if (tti.scan(rusWords.get(j)) != 1) {
			rusWord5 = new JLabel(rusWords.get(j).get(0) + ", " + rusWords.get(j).get(1));
		}
		rusWord5.setBounds(10, 386, 256, 28);
		rusWord5.setForeground(new Color(199, 21, 133));
		rusWord5.setFont(new Font("Tahoma", Font.PLAIN, 18));
		panel.add(rusWord5);
		englishAnswerWord.add(englishWord.get(j));
		System.out.println(englishAnswerWord.get(4));
		
		eng1 = new JTextField();
		eng1.setFont(new Font("Sitka Banner", Font.ITALIC, 16));
		eng1.setForeground(new Color(0, 0, 139));
		eng1.setBackground(new Color(255, 218, 185));
		eng1.setBounds(276, 230, 187, 28);
		panel.add(eng1);
		eng1.setColumns(10);
		
		eng2 = new JTextField();
		eng2.setForeground(new Color(0, 0, 139));
		eng2.setFont(new Font("Sitka Banner", Font.ITALIC, 16));
		eng2.setEditable(false);
		eng2.setBackground(new Color(255, 218, 185));
		eng2.setBounds(276, 269, 187, 28);
		panel.add(eng2);
		eng2.setColumns(10);
		
		eng3 = new JTextField();
		eng3.setForeground(new Color(0, 0, 128));
		eng3.setFont(new Font("Sitka Banner", Font.ITALIC, 16));
		eng3.setEditable(false);
		eng3.setBackground(new Color(255, 218, 185));
		eng3.setBounds(276, 308, 187, 28);
		panel.add(eng3);
		eng3.setColumns(10);
		
		eng4 = new JTextField();
		eng4.setForeground(new Color(0, 0, 128));
		eng4.setFont(new Font("Sitka Banner", Font.ITALIC, 16));
		eng4.setEditable(false);
		eng4.setBackground(new Color(255, 218, 185));
		eng4.setBounds(276, 347, 187, 28);
		panel.add(eng4);
		eng4.setColumns(10);
		
		eng5 = new JTextField();
		eng5.setForeground(new Color(0, 0, 128));
		eng5.setFont(new Font("Sitka Banner", Font.ITALIC, 16));
		eng5.setEditable(false);
		eng5.setBackground(new Color(255, 218, 185));
		eng5.setBounds(276, 386, 187, 28);
		panel.add(eng5);
		eng5.setColumns(10);
		
		btn1 = new JButton("OK");
		btn1.setFont(new Font("Tahoma", Font.BOLD, 14));
		btn1.setForeground(new Color(0, 0, 139));
		btn1.setBackground(new Color(255, 250, 205));
		btn1.setBounds(473, 233, 65, 23);
		panel.add(btn1);
		
		btn1.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (eng1.getText().isEmpty()){
					label.setText("Fill the line");
				} else {
					HashMap<String, ArrayList<String>> a = new HashMap<>();
					a.put(checkWordOnSpace(eng1.getText().toLowerCase()), rusWords.get(numbers.get(0)));
					checkUsersDict(a);
					if (checkWordOnSpace(eng1.getText().toLowerCase()).equals(englishAnswerWord.get(0))){
						score++;
					}
					label.setText("");
					btn1.setEnabled(false);
					btn2.setEnabled(true);
					eng1.setEditable(false);
					eng2.setEditable(true);
				}
			}	
		});
		
		btn2 = new JButton("OK");
		btn2.setBackground(new Color(255, 250, 205));
		btn2.setForeground(new Color(25, 25, 112));
		btn2.setFont(new Font("Tahoma", Font.BOLD, 14));
		btn2.setBounds(473, 272, 65, 23);
		btn2.setEnabled(false);
		panel.add(btn2);
		
		btn2.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (eng2.getText().isEmpty()){
					label.setText("Fill the line");
				} else {
					HashMap<String, ArrayList<String>> a = new HashMap<>();
					a.put(checkWordOnSpace(eng2.getText().toLowerCase()), rusWords.get(numbers.get(1)));
					checkUsersDict(a);
					if (checkWordOnSpace(eng2.getText().toLowerCase()).equals(englishAnswerWord.get(1))){
						score++;
					} 
					label.setText("");
					eng2.setEditable(false);
					eng3.setEditable(true);
					btn2.setEnabled(false);
					btn3.setEnabled(true);
				}	
			}	
		});
		
		btn3 = new JButton("OK");
		btn3.setForeground(new Color(25, 25, 112));
		btn3.setFont(new Font("Tahoma", Font.BOLD, 14));
		btn3.setBackground(new Color(255, 250, 205));
		btn3.setBounds(473, 311, 65, 23);
		btn3.setEnabled(false);
		panel.add(btn3);
		
		btn3.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (checkWordOnSpace(eng3.getText()).isEmpty()){
					label.setText("Fill the line");
				} else {
					HashMap<String, ArrayList<String>> a = new HashMap<>();
					a.put(checkWordOnSpace(eng3.getText().toLowerCase()), rusWords.get(numbers.get(2)));
					checkUsersDict(a);
					if (checkWordOnSpace(eng3.getText().toLowerCase()).equals(englishAnswerWord.get(2))){
						score++;
					}
					label.setText("");
					eng3.setEditable(false);
					eng4.setEditable(true);
					btn3.setEnabled(false);
					btn4.setEnabled(true);
				}
			}	
		});
		
		btn4 = new JButton("OK");
		btn4.setForeground(new Color(25, 25, 112));
		btn4.setBackground(new Color(255, 250, 205));
		btn4.setFont(new Font("Tahoma", Font.BOLD, 14));
		btn4.setBounds(473, 350, 65, 23);
		btn4.setEnabled(false);
		panel.add(btn4);
		
		btn4.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (checkWordOnSpace(eng4.getText()).isEmpty()){
					label.setText("Fill the line");
				} else {
					HashMap<String, ArrayList<String>> a = new HashMap<>();
					a.put(checkWordOnSpace(eng4.getText().toLowerCase()), rusWords.get(numbers.get(3)));
					checkUsersDict(a);
					if (checkWordOnSpace(eng4.getText().toLowerCase()).equals(englishAnswerWord.get(3))){
						score++;
					} 
					label.setText("");
					eng4.setEditable(false);
					eng5.setEditable(true);
					btn4.setEnabled(false);
					btn5.setEnabled(true);
				}
			}	
		});
		
		btn5 = new JButton("OK");
		btn5.setFont(new Font("Tahoma", Font.BOLD, 14));
		btn5.setBackground(new Color(255, 250, 205));
		btn5.setForeground(new Color(25, 25, 112));
		btn5.setBounds(473, 389, 65, 23);
		btn5.setEnabled(false);
		panel.add(btn5);
		
		btn5.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (eng5.getText().isEmpty()){
					label.setText("Fill the line");
				} else {
					HashMap<String, ArrayList<String>> a = new HashMap<>();
					a.put(checkWordOnSpace(eng5.getText().toLowerCase()), rusWords.get(numbers.get(4)));
					checkUsersDict(a);
					if (checkWordOnSpace(eng5.getText().toLowerCase()).equals(englishAnswerWord.get(4))){
						score++;
					} 
					label.setText("");
					eng5.setEditable(false);
					btn5.setEnabled(false);
					btnDone.setVisible(true);
				}
			}	
		});
		
		label = new JLabel("");
		label.setForeground(new Color(0, 0, 139));
		label.setFont(new Font("Ravie", Font.ITALIC, 15));
		label.setBounds(82, 425, 136, 28);
		panel.add(label);
		
		btnDone = new JButton("Done");
		btnDone.setBackground(new Color(255, 250, 205));
		btnDone.setForeground(new Color(0, 0, 205));
		btnDone.setFont(new Font("Tahoma", Font.BOLD, 14));
		btnDone.setBounds(276, 425, 136, 28);
		btnDone.setVisible(false);
		try {
			btnDone.setIcon(new ImageIcon(ImageIO.read(new File("Files/ic_done_black_24dp.png"))));
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		panel.add(btnDone);
		
		lblNewLabel = new JLabel("Answers");
		lblNewLabel.setForeground(new Color(0, 0, 139));
		lblNewLabel.setFont(new Font("Ravie", Font.BOLD, 18));
		lblNewLabel.setBounds(214, 11, 148, 24);
		lblNewLabel.setVisible(false);
		panel.add(lblNewLabel);
		
		Exit = new JButton("Back to menu");
		Exit.setBackground(new Color(255, 250, 205));
		Exit.setFont(new Font("Tahoma", Font.BOLD, 14));
		Exit.setForeground(new Color(0, 0, 128));
		Exit.setBounds(351, 425, 187, 28);
		Exit.setVisible(false);
		try {
			Exit.setIcon(new ImageIcon(ImageIO.read(new File("Files/ic_undo_black_48dp.png"))));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		panel.add(Exit);
		
		label_0 = new JLabel("");
		label_0.setBounds(548, 35, 24, 24);
		label_0.setBackground(SystemColor.menu);
		label_0.setIcon(new ImageIcon(ImageIO.read(new File("Files/back.png"))));
		label_0.setVisible(false);
		panel.add(label_0);
		
		label_1 = new JLabel("");
		label_1.setBackground(SystemColor.menu);
		label_1.setBounds(548, 74, 24, 24);
		label_1.setIcon(new ImageIcon(ImageIO.read(new File("Files/back.png"))));
		label_1.setVisible(false);
		panel.add(label_1);
		
		label_2 = new JLabel("");
		label_2.setBackground(SystemColor.menu);
		label_2.setBounds(548, 113, 24, 24);
		label_2.setIcon(new ImageIcon(ImageIO.read(new File("Files/back.png"))));
		label_2.setVisible(false);
		panel.add(label_2);
		
		label_3 = new JLabel("");
		label_3.setBackground(SystemColor.menu);
		label_3.setBounds(548, 152, 24, 24);
		label_3.setIcon(new ImageIcon(ImageIO.read(new File("Files/back.png"))));
		label_3.setVisible(false);
		panel.add(label_3);
		
		label_4 = new JLabel("");
		label_4.setBackground(SystemColor.menu);
		label_4.setBounds(548, 191, 24, 24);
		label_4.setIcon(new ImageIcon(ImageIO.read(new File("Files/back.png"))));
		label_4.setVisible(false);
		panel.add(label_4);
		
		btnDone.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				int j1 = 1;
				engWord1.setText(j1 + ") " + rusWord1.getText()); j1++;
				engWord2.setText(j1 + ") " + rusWord2.getText()); j1++;
				engWord3.setText(j1 + ") " + rusWord3.getText()); j1++;
				engWord4.setText(j1 + ") " + rusWord4.getText()); j1++;
				engWord5.setText(j1 + ") " + rusWord5.getText()); j1++;
				engWord6.setText(englishAnswerWord.get(0));
				engWord7.setText(englishAnswerWord.get(1));
				engWord8.setText(englishAnswerWord.get(2));
				engWord9.setText(englishAnswerWord.get(3));
				engWord10.setText(englishAnswerWord.get(4));
				label.setText("Score " + score);
				
				if (checkWordOnSpace(eng1.getText()).equals(englishAnswerWord.get(0))){
					label_0.setVisible(true);
				} 
				if (checkWordOnSpace(eng2.getText()).equals(englishAnswerWord.get(1))){
					label_1.setVisible(true);
				} 
				if (checkWordOnSpace(eng3.getText()).equals(englishAnswerWord.get(2))){
					label_2.setVisible(true);
				} 
				if (checkWordOnSpace(eng4.getText()).equals(englishAnswerWord.get(3))){
					label_3.setVisible(true);
				} 
				if (checkWordOnSpace(eng5.getText()).equals(englishAnswerWord.get(4))){
					label_4.setVisible(true);
				} 
				btnDone.setVisible(false);
				lblNewLabel.setVisible(true);
				Exit.setVisible(true);
			}
		});
		
		Exit.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				Main m = new Main();
				m.setUsername(username);
				m.change();
				m.upDate();
				score = 0;
				englishAnswerWord.clear();
				rusWords.clear();
				englishWord.clear();
				numbers.clear();
				frmThirdTest.dispose();
			}
		});
	}
	
	public void collectWords() {
	// ���������� ������� ���� � ������� ������ �� ���������
		ArrayList<HashMap<String, ArrayList<String>>> dict = tti.getDict();
		
		for (int i = 0; i < dict.size(); i++) {
			HashMap<String, ArrayList<String>> onePairOfWords = dict.get(i);
			Set<String> keys = onePairOfWords.keySet();
			for (String key: keys) { 
				englishWord.add(key);
				rusWords.add(onePairOfWords.get(key));
			}
		}
	}
	
	public int generateNumber() {
		// ��������� ������� ������� ���� � radiobutton
		Random r = new Random();
		int count = 0;
		do {
			count = (int) r.nextInt(englishWord.size());
		} while (numbers.contains(count));
		numbers.add(count);	
		return count;
	}
	
	public void change(String username) {
		frmThirdTest.setVisible(true);
		this.username = username;
	}
	
	public String checkWordOnSpace(String str) {
		System.out.println(str);
		char [] a = str.toCharArray();
		str = "";
		for (int i = 0; a.length > i; i++) {
			if (a[i] == ' ') {				
				continue;
			} else {
				str += a[i];
			}
		} 
		System.out.println(str);
		return str;
	}
	
	@SuppressWarnings("static-access")
	public void checkUsersDict(HashMap<String, ArrayList<String>> word) {
	// ������ ���� � ��������
		ArrayList<HashMap<String, ArrayList<String>>> a = Main.getLerning();
		System.out.println(word);
		System.out.println(a);
		if (a.contains(word)) {
			a.remove(word);
			a.add(d.getWordFromMainDictionary(new Random().nextInt(d.getLength())));

			ArrayList<HashMap<String, ArrayList<String>>> b = Main.getLerned();
			b.add(word);
			d.checkOnLearnedLearning();
			System.out.println("+");
		}
	}
}